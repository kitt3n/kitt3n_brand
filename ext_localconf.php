<?php
defined('TYPO3_MODE') || die('Access denied.');


## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
// ToDo: Resourcen von Grafikern erstellen lassen und in das angegebene Verzeichnis ablegen
$boot = function () {

    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_brand')) {

        #######################
        #### ICON REGISTRY ####
        #######################
        $oIconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Core\Imaging\IconRegistry::class
        );

        // SVGs
        $oIconRegistry->registerIcon(
            "kitt3n_svg_big",
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg']
        );

        // SVGs
        $oIconRegistry->registerIcon(
            "kitt3n_cat_svg_gray",
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/cat_gray_482x482.svg']
        );

        // SVGs
        $oIconRegistry->registerIcon(
            "kitt3n_cat_svg_orange",
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/cat_orange_482x482.svg']
        );

        // SVGs
        $oIconRegistry->registerIcon(
            "kitt3n_cat_svg_white",
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/cat_white_482x482.svg']
        );

        // PNGs
        $oIconRegistry->registerIcon(
            "kitt3n_png_small",
            \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
            ['source' => 'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/PNGs/kitten_482x482.png']
        );


        if (strpos(php_sapi_name(),'cli') !== false) {
        } else {
            ## Array of all background images
            $sPathToBackgroundImages = $_SERVER['DOCUMENT_ROOT']. '/typo3conf/ext/kitt3n_brand/Resources/Public/Assets/Backend/Backgrounds';
            $aImages = array_diff(scandir($sPathToBackgroundImages), array('..', '.'));

            ##########################
            #### BACKEND BRANDING ####
            ##########################
            $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginLogo'] = 'EXT:kitt3n_brand/Resources/Public/Assets/Backend/loginLogo.svg';
            $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginHighlightColor'] = '#f49700';
            $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginBackgroundImage'] = 'EXT:kitt3n_brand/Resources/Public/Assets/Backend/Backgrounds/' . $aImages[array_rand($aImages)];
            // $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginFootnote'] = '© TYPO3 Kit // kitt3n';
            $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['backendLogo'] = 'EXT:kitt3n_brand/Resources/Public/Assets/Backend/backendLogo.svg';
            $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['backendFavicon'] = 'EXT:kitt3n_brand/Resources/Public/Assets/Backend/backendFavicon.ico';
        }
    }

};

$boot();
unset($boot);